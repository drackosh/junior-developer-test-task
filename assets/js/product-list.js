$(document).ready(function () {
    $(document).on("click", "input[value=ADD]", function () {
        location.href = "./add-product";
    });

    $(document).on("click", "input#delete-product-btn", function () {
        var arr = [];
        $("input.delete-checkbox:checked").each(function () {
            arr.push($(this).closest("div.product").attr("sku"));
        });

        $.ajax({
            url: "./ajax/delete-products.php",
            method: "POST",
            data: JSON.stringify({ toDelete: arr }),
            cache: false,
            processData: false,
            contentType: 'application/json',
            success: function () {
                arr.forEach(deleteProduct);
                function deleteProduct(item, index) {
                    $("div.product[sku=" + item + "]").remove();
                }
                arr = [];
            }
        });
    });
});