$(document).ready(function () {
    $(document).on("click", "input[type=submit]", function () {
        var isValid = true;
        $("div.error").remove();
        $('#product_form input:visible').each(function () {
            if (!$.trim(this.value).length) {
                isValid = false;
                $(this).closest("div.input-container").after("<div class='error'>Please, submit required data!</div>")
            } else if ($(this).attr("type") == "number") {
                var reg = /^[1-9]\d*(\.\d+)?$/;
                if (!reg.test($(this).val())) {
                    isValid = false;
                    $(this).closest("div.input-container").after("<div class='error'>Please, provide the data of indicated type!</div>")
                }
            }
        });

        if (isValid) {
            $.ajax({
                url: "./ajax/add-product-submit.php",
                method: "POST",
                data: $("#product_form").find("input:visible, select#productType").serialize(),
                cache: false,
                processData: false,
                contentType: "application/x-www-form-urlencoded",
                success: function () {
                    location.href = "./";
                }
            });
        }
    });

    $(document).on("click", "input[value=Cancel]", function () {
        location.href = "./";
    });

    var visible;
    $(document).on('change', 'select#productType', function () {
        visible = this.value.toLowerCase();
        $(".type-input").css("display", "none");
        $("." + visible + "-type-input").css("display", "block");
    });
});
