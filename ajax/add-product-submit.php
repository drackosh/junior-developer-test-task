<?php
include '../classes/dbh.class.php';
include '../classes/product.class.php';
include '../classes/category.class.php';
if ($_SERVER["REQUEST_METHOD"] == "POST" && $_SERVER["CONTENT_TYPE"] == "application/x-www-form-urlencoded") {
    $category = new Category();
    $catId = $category->getCategoryId(strtolower($_POST['productType']));
    $product = new Product($_POST['sku'], $_POST['name'], $_POST['price'], $catId);
    $lastId = $product->insertProduct($_POST['sku'], $_POST['name'], $_POST['price'], $catId);
    $product->insertAttributes(strtolower($_POST['productType']), $lastId, array_slice($_POST, 4));
}
