<?php
include '../classes/dbh.class.php';
include '../classes/product.class.php';
if ($_SERVER["REQUEST_METHOD"] == "POST" && $_SERVER["CONTENT_TYPE"] == "application/json") {
    $rData = file_get_contents("php://input");
    $data = json_decode($rData);

    $product = new Product();
    foreach ($data->toDelete as $p) {
        $product->deleteProduct($p);
    }
}
