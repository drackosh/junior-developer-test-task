<?php
include './classes/dbh.class.php';
include './classes/product.class.php';
include './classes/category.class.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product List</title>
    <link rel="stylesheet" href="./assets/css/p-list.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
</head>

<body>
    <header>
        <h3>Product List</h3>
        <nav>
            <input type="button" class="btn btn-green" value="ADD">
            <input type="button" class="btn btn-red" id="delete-product-btn" value="MASS DELETE">
        </nav>
    </header>
    <main>
        <div class="products">
            <?php
            $product = new Product();
            $products = $product->getProducts();
            $category = new Category();
            foreach ($products as $p) { ?>
                <div class="product" sku="<?php echo $p->sku ?>">
                    <input type="checkbox" class="delete-checkbox">
                    <div class="sku"><?php echo htmlspecialchars($p->sku) ?></div>
                    <div class="name"><?php echo htmlspecialchars($p->name) ?></div>
                    <div class="price"><?php echo htmlspecialchars($p->price) . " $" ?></div>
                    <?php
                    $catName = $category->getCategoryName($p->category);
                    $attributes = $product->getAttributes(strtolower($catName), $p->id);

                    $size = array();
                    foreach ($attributes as $name => $value) {
                        if (!in_array($name, array("id", "fk_product"))) {
                            if (in_array($name, array("size", "height", "width", "length", "weight"))) {
                                $size += [$name => ($value)];
                            } else {
                                echo $name . ":" . htmlspecialchars($value);
                            }
                        }
                    }
                    echo $product->unit($size);
                    ?>
                </div>
            <?php } ?>
        </div>
    </main>
    <script src="./assets/js/product-list.js"></script>
</body>

</html>