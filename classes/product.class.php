<?php

class Product extends Dbh
{
    public $sku;
    public $name;
    public $price;
    public $category;

    public function getProducts()
    {
        $sql = "SELECT * FROM products";
        $stmt = $this->connect()->prepare($sql);

        if ($stmt->execute()) {
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }
    }

    public function getAttributes($categoryName, $productId)
    {
        $sql = "SELECT * FROM {$categoryName} WHERE fk_product = :fk_product";
        $stmt = $this->connect()->prepare($sql);

        $stmt->bindParam(':fk_product', $productId, PDO::PARAM_INT);

        if ($stmt->execute()) {
            return $stmt->fetch(PDO::FETCH_ASSOC);
        }
    }

    public function deleteProduct($sku)
    {
        $sql = "DELETE FROM products WHERE sku = :sku";
        $stmt = $this->connect()->prepare($sql);

        $stmt->bindParam(":sku", $sku);

        if ($stmt->execute()) {
            return true;
        }
    }

    public function insertProduct($sku, $name, $price, $categoryId)
    {
        $sql = "INSERT INTO products (sku, name, price, category)
        VALUES (:sku, :name, :price, :category)";
        $conn = $this->connect();
        $stmt = $conn->prepare($sql);

        $stmt->bindParam(':sku', $sku);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':price', $price);
        $stmt->bindParam(':category', $categoryId);

        if ($stmt->execute()) {
            return $conn->lastInsertId();
        }
    }

    public function insertAttributes($category, $fkProduct, $attributes)
    {
        $columns = implode(', ', array_keys($attributes));
        $values = ':' . implode(', :', array_keys($attributes));

        $sql = "INSERT INTO {$category} (fk_product, {$columns}) VALUES (:fk_product, {$values})";

        if ($stmt = $this->connect()->prepare($sql)) {
            $stmt->bindValue(':fk_product', $fkProduct);
            foreach ($attributes as $name => $value) {
                $stmt->bindValue(':' . $name, $value);
            }
        }

        if ($stmt->execute()) {
            return true;
        }
    }

    public function unit($array)
    {
        if (count($array) == 3) {
            return "Dimensions: " . array_values($array)[0] . "x" . array_values($array)[1] . "x" . array_values($array)[2];
        } else if (count($array) == 1) {
            switch (array_keys($array)[0]) {
                case "weight":
                    return "Weight: " . array_values($array)[0] . " KG";
                    break;
                case "size":
                    return "Size: " . array_values($array)[0] . " MB";
                    break;
            }
        }
    }
}
