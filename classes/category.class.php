<?php

class Category extends Dbh
{
    public function getCategories()
    {
        $sql = "SELECT name FROM categories";
        $stmt = $this->connect()->prepare($sql);

        if ($stmt->execute()) {
            $results = $stmt->fetchAll(PDO::FETCH_OBJ);
            return $results;
        }
    }

    public function getCategoryId($catName)
    {
        $sql = "SELECT id FROM categories WHERE name = :name";
        $stmt = $this->connect()->prepare($sql);

        $stmt->bindValue(':name', $catName);

        if ($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_COLUMN);
            return $result;
        }
    }

    public function getCategoryName($catId)
    {
        $sql = "SELECT name FROM categories WHERE id = :id";
        $stmt = $this->connect()->prepare($sql);

        $stmt->bindParam(':id', $catId, PDO::PARAM_INT);

        if ($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_COLUMN);
            return $result;
        }
    }
}
