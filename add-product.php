<?php
include './classes/dbh.class.php';
include './classes/product.class.php';
include './classes/category.class.php';
$category = new Category();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product List</title>
    <link rel="stylesheet" href="./assets/css/add-product.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
</head>

<body>
    <header>
        <h3>Product Add</h3>
        <nav>
            <input type="submit" class="btn btn-green" value="Save">
            <input type="button" class="btn btn-red" value="Cancel">
        </nav>
    </header>
    <main>
        <form id="product_form" method="POST">
            <div class="sku-input input-container">
                <label for="sku">SKU</label>
                <input type="text" name="sku" id="sku" required>
            </div>
            <div class="name-input input-container">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" required>
            </div>
            <div class="price-input input-container">
                <label for="price">Price ($)</label>
                <input type="number" name="price" id="price" min="0" required>
            </div>
            <div class="type-switcher input-container">
                <label for="productType">Type Switcher</label>
                <select id="productType" name="productType">
                    <option selected="true" disabled="disabled">Choose Type</option>
                    <?php
                    foreach ($category->getCategories() as $cat) {
                        echo "<option value='$cat->name'>" . ucfirst($cat->name) . "</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="book-type-input type-input">
                <div class="weight-input input-container">
                    <label for="weight">Weight (KG)</label>
                    <input type="number" name="weight" min="0" id="weight" required>
                </div>
                <span class="indications">Please provide weight</span>
            </div>
            <div class="dvd-type-input type-input">
                <div class="size-input input-container">
                    <label for="size">Size (MB)</label>
                    <input type="number" name="size" min="0" id="size" required>
                </div>
                <span class="indications">Please provide size</span>
            </div>
            <div class="furniture-type-input type-input">
                <div class="height-input input-container">
                    <label for="height">Height (CM)</label>
                    <input type="number" name="height" min="0" id="height" required>
                </div>
                <div class="width-input input-container">
                    <label for="width">Width (CM)</label>
                    <input type="number" name="width" min="0" id="width" required>
                </div>
                <div class="length-input input-container">
                    <label for="length">Length (CM)</label>
                    <input type="number" name="length" min="0" id="length" required>
                </div>
                <span class="indications">Please provide dimensions</span>
            </div>
        </form>
    </main>
    <script src="./assets/js/add-product.js"></script>
</body>

</html>